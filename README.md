# How to use

1. Install and configure bakthat

        pip install bakthat
        bakthat configure
       

2. Install the requirements

        pip install -r requirements.txt


3. Enter the databases you want to backup into the backup.yml

		servers:
		    - name: myfirstserver
		      host: 123.123.123.123
		      user: root
		      password: 1234
		      databases:
		            - db1
		            - db2
		            - db3

                    
4. Start backing up!

		python backup.py
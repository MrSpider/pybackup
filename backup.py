import logging
import sh
from bakthat.helper import BakHelper

# Set up yaml loaders
import yaml
try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader

logging.basicConfig(level=logging.WARNING)
CONFIG_FILE = 'backup.yml'
logger = logging.getLogger('pybackup')


# The bakthat config is in ~/.bakthat.yml
def backup_database(name, host, user, password, db):
    with BakHelper(name + '__' + db, tags=["mysql"]) as bh:
        sh.mysqldump(db,
                     '-p{0}'.format(password),
                     u=user,
                     h=host,
                     _out='dump.sql')
        bh.backup()


def backup_server(server):
    logger.info("Backing up {} databases on {}".format(len(server['databases']), server['name']))
    for db in server['databases']:
        backup_database(server['name'], server['host'], server['user'], server['password'], db)


def backup_from_config(config):
    for server in config['servers']:
        backup_server(server)


def load_config():
    with open(CONFIG_FILE, 'r') as f:
        return yaml.load(f, Loader=Loader)


if __name__ == "__main__":
    backup_from_config(load_config())